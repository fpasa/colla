name := "Colla"
version := "1.1.1"
maintainer := "Francesco Pasa <francescopasa@gmail.com>"
packageSummary := "Generates photo collages"
packageDescription := "Colla allows you to generate collages from your photos, and comes with a variety of templates and options."
rpmVendor := "fpasa.xyz"
rpmLicense := Some("MIT")

scalaVersion := "3.2.2"

lazy val root = (project in file("."))
  .settings(
    name := "photo-collator",
    idePackagePrefix := Some("xyz.fpasa.colla")
  )

libraryDependencies += "org.scala-lang.modules" %% "scala-swing" % "3.0.0"
libraryDependencies += "com.formdev" % "flatlaf" % "3.0"

enablePlugins(JavaAppPackaging)
enablePlugins(DebianPlugin)
enablePlugins(RpmPlugin)
enablePlugins(WindowsPlugin)
enablePlugins(JDKPackagerPlugin)

debianPackageDependencies := Seq("java8-runtime-headless")
jdkAppIcon := "icon.ico"