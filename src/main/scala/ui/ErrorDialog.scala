package xyz.fpasa.colla
package ui

import javax.swing.JOptionPane

object ErrorDialog {
  def showError(message: String, err: Exception): Unit = {
    JOptionPane.showMessageDialog(
      null, err.getMessage, message, JOptionPane.ERROR_MESSAGE)
  }
}
