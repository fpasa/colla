package xyz.fpasa.colla.ui

import javax.swing.JOptionPane
import scala.swing.*
import scala.swing.event.*

class CollaMenuBar(
  val reset: () => Unit,
  val openPhotos: () => Unit,
  val save: () => Unit,
  val quit: () => Unit,
) extends MenuBar {
  private val fileSaveButton = new MenuItem("Save") {
    mnemonic = Key.S
    enabled = false
    reactions += { case ButtonClicked(_) => save() }
  }

  contents += new Menu("File") {
    mnemonic = Key.F
    contents ++= Seq(
      new MenuItem("New collage") {
        mnemonic = Key.N
        reactions += { case ButtonClicked(_) => reset() }  // TODO: confirm
      },
      new MenuItem("Import photos") {
        mnemonic = Key.I
        reactions += { case ButtonClicked(_) => openPhotos() }
      },
      fileSaveButton,
      new Separator,
      new MenuItem("Quit") {
        mnemonic = Key.Q
        reactions += { case ButtonClicked(_) => quit() }
      },
    )
  }

  contents += new Menu("Help") {
    mnemonic = Key.H
    contents ++= Seq(
      new MenuItem("About") {
        reactions += { case ButtonClicked(_) =>
          JOptionPane.showMessageDialog(
            null,
            "Colla allows you to generate collages from your photos,\n" +
              "and comes with a variety of templates and options.\n \n" +
              "This program was written the old fashioned way:\n" +
              "simple, handcrafted code aged in ancient oak barrels.\n" +
              "No network required\n \n" +
              "Written by Francesco Pasa - Anno 2023\n",
            s"Colla ${getClass.getPackage.getImplementationVersion}",
            JOptionPane.PLAIN_MESSAGE,
            Swing.Icon("icon.png"),
          )
        }
      },
    )
  }

  def onNumPhotosChanged(num: Int): Unit =
      fileSaveButton.enabled = num > 0
}