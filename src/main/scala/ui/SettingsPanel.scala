package xyz.fpasa.colla
package ui

import generators.{Setting, Settings}
import ui.SettingsPanel.slider

import java.awt.Color
import java.awt.event.ItemListener
import javax.swing.border.EmptyBorder
import javax.swing.{BorderFactory, JPanel}
import scala.swing.event.{ButtonClicked, ColorChanged, ValueChanged}
import scala.swing.*

object SettingsPanel {
  private val LARGE_NUMBER = 65_536;

  def generate(settings: Settings, refreshCollage: () => Unit): BoxPanel =
    new BoxPanel(Orientation.Vertical) {
      contents ++= settings.flatMap(s => s.value match {
        case _: Boolean => withStrut(checkbox(s, refreshCollage))
        case _: Int => withStrut(slider(s, refreshCollage))
        case _: Color => withStrut(colorChooser(s, refreshCollage))
      })
      contents += Swing.Glue
    }

  private def withStrut(panel: SettingPanel) =
    Seq(panel, Swing.VStrut(24))

  private def checkbox(setting: Setting, refreshCollage: () => Unit): SettingPanel =
    new SettingPanel {
      private val checkBox = new CheckBox(setting.name) {
        selected = setting.value.asInstanceOf[Boolean]
        tooltip = setting.description
        reactions += { case ButtonClicked(_) =>
          refreshCollage()
        }
      }

      override def widgetValue: Boolean = checkBox.selected

      contents += checkBox
    }

  private def slider(setting: Setting, refreshCollage: () => Unit): SettingPanel =
    new SettingPanel {
      border = BorderFactory.createTitledBorder(setting.name)

      private var last = setting.value.asInstanceOf[Int];

      private val slider: Slider = new Slider {
        border = EmptyBorder(8, 8, 8, 8)
        min = 0
        max = 256
        majorTickSpacing = 32
        minorTickSpacing = 8
        paintLabels = true
        paintTicks = true
        snapToTicks = true
        value = setting.value.asInstanceOf[Int]
        tooltip = setting.description
        reactions += { case ValueChanged(_) =>
          // For some reason the slider causes some jitter
          // and sometimes assigns invalid values which are
          // not multiple of 8
          if (last != value && value % 8 == 0) {
            last = value
            refreshCollage()
          }
          value = last
        }
      }

      override def widgetValue: Int = slider.value.toInt

      contents += slider
    }

  private def colorChooser(setting: Setting, refreshCollage: () => Unit): SettingPanel =
    new SettingPanel {
      border = BorderFactory.createTitledBorder(setting.name)
      private val colorChooser: ColorChooser = new ColorChooser {
        border = EmptyBorder(8, 8, 8, 8)
        color = setting.value.asInstanceOf[Color]
        tooltip = setting.description
        maximumSize = new Dimension(LARGE_NUMBER, preferredSize.getHeight.toInt)
        // Removes preview panel
        peer.setPreviewPanel(new JPanel())
        reactions += { case ColorChanged(_, _) =>
          refreshCollage()
        }
      }

      override def widgetValue: Color = colorChooser.color

      contents += colorChooser
    }

  def getSettings(panel: BoxPanel, defaultSettings: Settings): Settings = {
    val settings = panel.contents
      .filter(panel => panel.isInstanceOf[SettingPanel])
      .map(panel => panel.asInstanceOf[SettingPanel].widgetValue)
      .zip(defaultSettings)
      .map((v, s) => v match {
        case value: Boolean => {
          assert(
            s.value.isInstanceOf[Boolean],
            s"Setting value '${s.value}' not matching widget value '${value}''",
          )
          s.withValue(value)
        }
        case value: Int =>  {
          assert(
            s.value.isInstanceOf[Int],
            s"Setting value '${s.value}' not matching widget value '${value}''",
          )
          s.withValue(value)
        }
        case value: Color => {
          assert(
            s.value.isInstanceOf[Color],
            s"Setting value '${s.value}' not matching widget value '${value}''",
          )
          s.withValue(value)
        }
      })
      .toSeq
    Settings(settings)
  }
}

abstract class SettingPanel extends BoxPanel(Orientation.Horizontal) {
  xLayoutAlignment = 0

  def widgetValue: Any
}
