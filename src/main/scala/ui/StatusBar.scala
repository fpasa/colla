package xyz.fpasa.colla
package ui

import javax.swing.border.EmptyBorder
import scala.swing.*

class StatusBar(message: String = "") extends BoxPanel(Orientation.Horizontal) {
  border = new EmptyBorder(8, 8, 8, 8)

  private val countLabel: Label = new Label(message)
  val progressLabel: Label = new Label { visible = false }
  val progressBar: ProgressBar = new ProgressBar {
    minimumSize = new Dimension(320, 8)
    maximumSize = new Dimension(320, 8)
    visible = false
  }

  contents ++= Seq(
    countLabel,
    Swing.Glue,
    progressLabel,
    Swing.HStrut(8),
    progressBar,
  )

  def setMessage(message: String): Unit = countLabel.text = message;
}
