package xyz.fpasa.colla
package ui

import generators.{CollageGenerators, Settings, UICollageGenerator}

import java.awt.Color
import java.awt.event.InputEvent
import java.text.NumberFormat
import javax.swing.BorderFactory
import javax.swing.border.EmptyBorder
import javax.swing.text.DefaultFormatterFactory
import scala.swing.*
import scala.swing.event.{ButtonClicked, SelectionChanged, ValueChanged}

class SideBar(
  val refreshCollage: () => Unit,
  val shuffle: () => Unit,
) extends BoxPanel(Orientation.Vertical) {
  private val LARGE_NUMBER = 65_536;

  var selectedUIGenerator: UICollageGenerator = CollageGenerators.AvailableCollages.head
  var collageSize: (Int, Int) = (1920, 1920)

  private val collageList = new ListView[String](getCollageNames) {
    listenTo(selection)
    reactions += { case SelectionChanged(_) => {
      selectedUIGenerator = CollageGenerators.AvailableCollages(selection.leadIndex)
      updateSettingsPanel()
      refreshCollage()
    }}
  }
  private var settingsPanel = getSettingsPanel

  border = new EmptyBorder(8, 8, 8, 8)

  contents ++= Seq(
    new BoxPanel(Orientation.Horizontal) {
      xLayoutAlignment = 0
      contents ++= Seq(
        new Label("Collage size (pixels): "),
        new TextField(collageSize(0).toString) {
          maximumSize = new Dimension(LARGE_NUMBER, preferredSize.getHeight.toInt)
          reactions += { case ValueChanged(_) => {
            collageSize = (text.toIntOption.getOrElse(collageSize(0)), collageSize(1))
            refreshCollage()
          }}
        },
        new Label(" × "),
        new TextField(collageSize(1).toString) {
          maximumSize = new Dimension(LARGE_NUMBER, preferredSize.getHeight.toInt)
          reactions += { case ValueChanged(_) => {
            collageSize = (collageSize(0), text.toIntOption.getOrElse(collageSize(1)))
            refreshCollage()
          }}
        },
      )
    },
    Swing.VStrut(24),
    new ScrollPane(collageList) {
      xLayoutAlignment = 0
      maximumSize = new Dimension(LARGE_NUMBER, 640)
    },
    Swing.VStrut(24),
    settingsPanel,
    Swing.Glue,
    new Button("Shuffle") {
      maximumSize = new Dimension(LARGE_NUMBER, 48)
      reactions += { case ButtonClicked(_) =>
        shuffle()
        refreshCollage()
      }
    }
  )

  private def getCollageNames: Array[String] =
    CollageGenerators.AvailableCollages.map(c => c.name).toArray

  private def getSettingsPanel: BoxPanel =
    SettingsPanel.generate(selectedUIGenerator.defaultOptions, refreshCollage)

  private def updateSettingsPanel(): Unit = {
    settingsPanel = getSettingsPanel
    contents(contents.length - 3) = settingsPanel
    revalidate()
    repaint()
  }

  def getCollageSettings: Settings =
    SettingsPanel.getSettings(settingsPanel, selectedUIGenerator.defaultOptions)
}
