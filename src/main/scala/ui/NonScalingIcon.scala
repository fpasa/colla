package xyz.fpasa.colla
package ui

import java.awt.*
import java.awt.geom.AffineTransform
import javax.swing.*

class NoScalingIcon(private var icon: Icon) extends Icon {
  override def getIconWidth: Int = icon.getIconWidth
  override def getIconHeight: Int = icon.getIconHeight

  override def paintIcon(c: Component, g: Graphics, x: Int, y: Int): Unit = {
    val g2d = g.create.asInstanceOf[Graphics2D]
    val at = g2d.getTransform
    val scaleX = (x * at.getScaleX).toInt
    val scaleY = (y * at.getScaleY).toInt
    val offsetX = (icon.getIconWidth * (at.getScaleX - 1) / 2).toInt
    val offsetY = (icon.getIconHeight * (at.getScaleY - 1) / 2).toInt
    val locationX = scaleX + offsetX
    val locationY = scaleY + offsetY
    at.setToScale(1, 1)
    g2d.setTransform(at)
    icon.paintIcon(c, g2d, 0, 0)
    g2d.dispose()
  }
}