package xyz.fpasa.colla
package ui

import java.awt.Color
import java.awt.image.BufferedImage
import javax.swing.ImageIcon
import scala.swing.*

class CollageImage(openPhotos: () => Unit = () => {}) extends BorderPanel {
  protected val DARK_GRAY = new Color(47, 49, 51)

  private val actionButtonPanel = new CenterPanel(
    Button("Import photos"){ openPhotos() }
  ) { background = DARK_GRAY }
  private val collageImage = Label()
  private val collagePanel = new ScrollPane(collageImage)

  layout(actionButtonPanel) = BorderPanel.Position.Center

  def setImage(image: BufferedImage): Unit = {
    layout(collagePanel) = BorderPanel.Position.Center
    collageImage.icon = ImageIcon(image)
  }

  def reset(): Unit = {
    layout(actionButtonPanel) = BorderPanel.Position.Center
    collageImage.icon = null
    revalidate()
    repaint()
  }
}
