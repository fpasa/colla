package xyz.fpasa.colla
package ui

import java.awt.Color
import scala.swing.*

class CenterPanel(val widget: Component) extends BoxPanel(Orientation.Vertical) {
  contents ++= Seq(
    Swing.Glue,
    new BoxPanel(Orientation.Horizontal) {
      contents ++= Seq(Swing.Glue, widget, Swing.Glue)
    },
    Swing.Glue,
  )

  override def background_=(color: Color): Unit = {
    super.background = color
    contents(1).background = color
  }
}
