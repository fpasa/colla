package xyz.fpasa.colla

import javax.swing.SwingWorker
import scala.annotation.tailrec
import scala.swing.*
import scala.swing.event.*

abstract class LongTask[T] extends SwingWorker[T, Unit] {
  private var _label: Option[Label] = None
  def label: Label = _label.orNull
  protected def label_=(l: Label): Unit = _label = Some(l)

  private var _progressBar: Option[ProgressBar] = None
  def progressBar: ProgressBar = _progressBar.orNull
  protected def progressBar_=(pb: ProgressBar): Unit = _progressBar = Some(pb)

  def start(): Unit = {
    // On execution, reset state and show widgets
    label.text = ""
    label.visible = true
    progressBar.value = 0
    progressBar.visible = true

    super.execute()
  }

  addPropertyChangeListener(evt => {
    // Update progress bar if the backgroundFunc calls
    if (evt.getPropertyName == "progress") {
      progressBar.value = evt.getNewValue.asInstanceOf[Integer]
    } else if (evt.getPropertyName == "statusMessage") {
      label.text = evt.getNewValue.asInstanceOf[String]
    }
  });

  // API to facilitate setting message in the status bar
  protected def statusMessage(message: String): Unit = {
    this.firePropertyChange("statusMessage", "", message)
  }

  override def done(): Unit = {
    // Facilitate working with values returned by the background process
    onDone(get())

    // On done, hide widgets again
    label.visible = false
    progressBar.visible = false
  }

  def onDone(value: T): Unit = {}
}