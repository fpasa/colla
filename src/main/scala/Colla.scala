package xyz.fpasa.colla

import Colla.{collageImage, photos}
import ui.*

import com.formdev.flatlaf.FlatDarkLaf
import com.formdev.flatlaf.ui.FlatProgressBarUI
import com.formdev.flatlaf.util.SystemInfo

import java.awt.Color
import java.awt.image.BufferedImage
import java.beans.{PropertyChangeEvent, PropertyChangeListener}
import java.io.File
import java.sql.Time
import java.util.concurrent.Executors
import javax.imageio.ImageIO
import javax.swing.*
import javax.swing.border.{Border, EmptyBorder}
import javax.swing.filechooser.{FileFilter, FileNameExtensionFilter}
import javax.swing.text.html.StyleSheet.BoxPainter
import scala.swing.*
import scala.swing.event.*
import scala.util.{Random, control}

object Colla extends SimpleSwingApplication {
  private var seed: Long = System.currentTimeMillis()
  private var photos: Seq[BufferedImage] = Seq()
  private var collage: Option[BufferedImage] = None

  private var collaMenuBar = new CollaMenuBar(
    reset = reset,
    openPhotos = openPhotos,
    save = save,
    quit = quit,
  )
  private var collageImage = CollageImage()
  private var sideBar = SideBar(refreshCollage, shuffle)
  private var statusBar = StatusBar()

  def top: Frame = new MainFrame {
    title = "Colla"
    iconImage = Swing.Icon("icon.png").getImage
    minimumSize = new Dimension(800, 600)

    collaMenuBar = new CollaMenuBar(
      reset = reset,
      openPhotos = openPhotos,
      save = save,
      quit = quit,
    )
    menuBar = collaMenuBar

    contents = new BorderPanel {
      collageImage = CollageImage(openPhotos)
      sideBar = SideBar(refreshCollage, shuffle)
      statusBar = StatusBar(statusBarCountText)

      layout(new SplitPane {
        orientation = Orientation.Vertical
        leftComponent = collageImage
        rightComponent = sideBar
        resizeWeight = 1
      }) = BorderPanel.Position.Center
      layout(statusBar) = BorderPanel.Position.South
    }

    maximize()
  }

  // This allows to customize some theming at startup
  override def startup(args: Array[String]): Unit = {
    // Use Laf for theming
    UIManager.setLookAndFeel(FlatDarkLaf())

    // Enable custom window decorations on linux
    if (SystemInfo.isLinux) {
      JFrame.setDefaultLookAndFeelDecorated(true)
      JDialog.setDefaultLookAndFeelDecorated(true)
    }

    // Some other theme customizations
    UIManager.put("Component.focusWidth", 2)
    UIManager.put("Component.hideMnemonics", false)

    super.startup(args)
  }

  private def openPhotos(): Unit = {
    val dialog = new JFileChooser()
    dialog.setFileFilter(new FileFilter {
      override val getDescription = "Image Files"
      override def accept(file: File): Boolean =
        file.isDirectory ||
        file.getPath.toLowerCase.endsWith(".png") ||
        file.getPath.toLowerCase.endsWith(".gif") ||
        file.getPath.toLowerCase.endsWith(".jpg") ||
        file.getPath.toLowerCase.endsWith(".jpeg") ||
        file.getPath.toLowerCase.endsWith(".tif") ||
        file.getPath.toLowerCase.endsWith(".tiff")
    })
    dialog.setMultiSelectionEnabled(true)
    dialog.showOpenDialog(null)
    loadPhotos(dialog.getSelectedFiles)
  }

  private def loadPhotos(files: Array[File]): Unit = {
    val task = new LongTask[Seq[BufferedImage]] {
      label = statusBar.progressLabel
      progressBar = statusBar.progressBar

      def doInBackground(): Seq[BufferedImage] = {
        val processors = Runtime.getRuntime.availableProcessors
        val threadPool = Executors.newFixedThreadPool(processors);

        statusMessage("Importing photos")
        files
          .map(file => threadPool.submit(() => ImageIO.read(file)))
          .zipWithIndex
          .map((future, i) => {
            setProgress(Math.round((i + 1).toFloat / files.length * 100))
            future.get()
          })
      }

      override def onDone(loadedPhotos: Seq[BufferedImage]): Unit = {
        photos = photos.appendedAll(loadedPhotos)
        collaMenuBar.onNumPhotosChanged(photos.length)
        statusBar.setMessage(statusBarCountText)
        SwingUtilities.invokeLater(() => refreshCollage())
      }
    }
    task.start()
  }

  private def statusBarCountText =
    s"Num. photos: ${photos.length.toString}"

  private def refreshCollage(): Unit = {
    if (photos.isEmpty) {
      collageImage.reset()
      return
    }

    val task = new LongTask[BufferedImage] {
      label = statusBar.progressLabel
      progressBar = statusBar.progressBar

      def doInBackground(): BufferedImage = {
        statusMessage("Generating collage")
        val uiGenerator = sideBar.selectedUIGenerator
        val generator = uiGenerator.newInstance(
          sideBar.collageSize,
          photos,
          sideBar.getCollageSettings,
        )
        generator.setSeed(seed)
        generator.createCollage()
      }

      override def onDone(image: BufferedImage): Unit =
        collage = Some(image)
        collageImage.setImage(image)
    }
    task.start()
  }

  private def reset(): Unit = {
    photos = Seq()
    collaMenuBar.onNumPhotosChanged(photos.length)
    statusBar.setMessage(statusBarCountText)
    refreshCollage()
  }

  private def save(): Unit = {
    val jpegFilter = new FileNameExtensionFilter("JPEG File", "jpeg")
    val pngFilter = new FileNameExtensionFilter("PNG File", "png")

    val dialog = new JFileChooser()
    dialog.setFileFilter(jpegFilter)
    dialog.setFileFilter(pngFilter)
    dialog.showSaveDialog(null)

    var path = dialog.getSelectedFile
    val normFilePath = path.toString.toLowerCase()
    if (dialog.getFileFilter == jpegFilter && !normFilePath.endsWith("jpeg") && !normFilePath.endsWith(".jpg")) {
      path = new File(path.toString + ".jpg")
      saveCollage(path, "jpg")
    } else if (dialog.getFileFilter == pngFilter && !normFilePath.endsWith("png")) {
      path = new File(path.toString + ".png")
      saveCollage(path, "png")
    }
  }

  private def saveCollage(path: File, format: String): Unit = {
    try {
      ImageIO.write(collage.get, format, path)
    } catch {
      case err: Exception => ErrorDialog.showError("Cannot write image", err)
    }
  }

  private def shuffle(): Unit = {
    seed = System.currentTimeMillis()
    photos = Random.shuffle(photos)
  }
}
