package xyz.fpasa.colla
package generators

import java.awt.Color
import java.awt.image.BufferedImage
import scala.util.Random

case class Block(
  pos: Vec2Di,
  size: Vec2Di,
) extends Ordered[Block] {
  def split(ratio: Float, gap: Int): Seq[Block] = {
    val halfGap = gap / 2;

    if (size.isHorizontal) {
      val start = Math.round(size.w * ratio)
      Seq(
        Block(pos, size.withX(start - halfGap)),
        Block(pos.withXOffset(start + halfGap), size.withXOffset(- start - halfGap)),
      )
    } else {
      val start = Math.round(size.h * ratio)
      Seq(
        Block(pos, size.withY(start - halfGap)),
        Block(pos.withYOffset(start + halfGap), size.withYOffset(- start - halfGap)),
      )
    }
  }

  override def compare(other: Block): Int = size.area - other.size.area
}

class HarmonicGenerator(
   _collageSize: (Int, Int),
   photos: Seq[BufferedImage],
   settings: Settings = DefaultSettings,
 ) extends CollageGenerator(_collageSize, photos, settings) {
  private var seed: Long = System.currentTimeMillis()
  private var layout: Seq[Block] = Seq()

  override def setSeed(_seed: Long): Unit = seed = _seed

  def createCollageBuffer(collage: BufferedImage): Unit = {
    Random.setSeed(seed)
    layoutPhotos()
    drawPhotos(collage)
  }

  private def layoutPhotos(): Unit = {
    var ratios = Seq(0.5f)
    if (settings.flagEnabled("useGoldenRatioSplits")) {
      ratios ++= Seq(0.618f, 0.382f)
    }
    if (settings.flagEnabled("useThirdsSplits")) {
      ratios ++= Seq(0.66f, 0.33f)
    }

    val margin = settings.getNumber("margin")
    val gap = settings.getNumber("gap")

    val initialBlock = Block(Vec2Di(margin, margin), collageSize - 2*margin)

    val blocks = collection.mutable.PriorityQueue(initialBlock)
    while (blocks.length < photos.length) {
      val blockToSplit = blocks.dequeue
      val ratio = OtherHelpers.choose(ratios)
      blocks ++= blockToSplit.split(ratio, gap)
    }

    layout = blocks.dequeueAll
  }

  private def drawPhotos(collage: BufferedImage): Unit = {
    val borderRadius = settings.getNumber("borderRadius")

    withGraphics(collage) { graphics =>
      val backgroundColor = settings.getColor("backgroundColor")
      graphics.fillBackground(backgroundColor, collageSize)

      layout
        .zip(photos)
        .foreach { (block, photo) =>
          val clipShape = graphics.roundedRectangle(block.pos, block.size, borderRadius)
          graphics.fitInto(photo, block.pos, block.size, clipShape = Some(clipShape))
        }
    }
  }
}

object HarmonicGenerator {
  val DefaultSettings: Settings = Settings(
    Constants.CommonGeneratorSettings
      ++
    Seq(
      Setting(
        "borderRadius", 0,
        "Border radius",
        "Radius of picture rounded corners. Set to zero to have rectangular photos."
      ),
      Setting(
        "useGoldenRatioSplits", true,
        "Golden ratio splits", "Enables splitting space using the golden ration (0.618)."
      ),
      Setting(
        "useThirdsSplits", false,
        "Thirds splits", "Enables splitting space in thirds."
      ),
    )
  )
}