package xyz.fpasa.colla
package generators

import java.awt.Color
import java.awt.image.BufferedImage
import scala.swing.Color

class GridRhombusGenerator(
  _collageSize: (Int, Int),
  photos: Seq[BufferedImage],
  settings: Settings = DefaultSettings,
) extends CollageGenerator(_collageSize, photos, settings) {
  def createCollageBuffer(collage: BufferedImage): Unit = {
    val numImages = findNumImagesAlongEdges
    layoutPhotos(collage, numImages)
  }

  protected def findNumImagesAlongEdges: Vec2Di = {
    var numImages = 1
    while (numImages*numImages < photos.length) {
      numImages += 1
    }

    if (settings.flagEnabled("expandPrevalentDirection")) {
      // In most cases, using the square gives us just
      // an initial estimation of the right dimensions.
      //
      // Subtract in one dimension and if it still contains
      // all images, subtract in the prevalent direction,
      // to improve use of available space.
      if ((numImages - 1) * numImages >= photos.length) {
        if (isPrevalentDirectionHorizontal) {
          return Vec2Di(numImages-1, numImages)
        } else {
          return Vec2Di(numImages, numImages-1)
        }
      }
    }

    Vec2Di(numImages, numImages)
  }

  private def isPrevalentDirectionHorizontal = {
    val countHorizontal = photos.foldLeft(0)(
      (acc, p) => acc + (if (aspectRatio(p) >= 1) 1 else 0)
    )
    val countVertical = photos.length - countHorizontal
    countHorizontal >= countVertical
  }

  private def aspectRatio(photo: BufferedImage): Float =
    photo.getWidth / photo.getHeight

  private def layoutPhotos(collage: BufferedImage, numImages: Vec2Di): Unit = {
    val margin = settings.getNumber("margin")
    val gap = settings.getNumber("gap")
    val borderRadius = settings.getNumber("borderRadius")

    val step = (collageSize - 2*margin + gap) / numImages
    val size = step - gap

    withGraphics(collage) { graphics =>
      val backgroundColor = settings.getColor("backgroundColor")
      graphics.fillBackground(backgroundColor, collageSize)

      numImages
        .iterMax(photos.length)
        .zip(photos)
        .foreach { (xy, photo) =>
          val pos = xy*step + margin
          val clipShape = graphics.roundedRectangle(pos, size, borderRadius)
          graphics.fitInto(photo, pos, size, clipShape = Some(clipShape))
        }
    }
  }
}

object GridRhombusGenerator {
  val DefaultSettings: Settings = Settings(
    Constants.CommonGeneratorSettings ++
    Seq(
      Setting(
        "borderRadius", 0,
        "Border radius",
        "Radius of picture rounded corners. Set to zero to have rectangular photos."
      ),
      Setting(
        "expandPrevalentDirection", true,
        "Expand photos", "Expand photos to better fill the available space."
      ),
    )
  )
}