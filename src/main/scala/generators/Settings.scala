package xyz.fpasa.colla
package generators

import java.awt.Color
import scala.collection.mutable

class Settings(val settings: Seq[Setting] = Seq()) extends Iterable[Setting] {
  override def iterator: Iterator[Setting] = settings.iterator

  private def getOption(key: String) =
    settings.find(opt => opt.key == key).get

  def flagEnabled(key: String): Boolean = {
    val option = getOption(key)
    assert(option.value.isInstanceOf[Boolean], s"Settings '$key' is not boolean!")
    option.value.asInstanceOf[Boolean]
  }

  def getNumber(key: String): Int = {
    val option = getOption(key)
    assert(option.value.isInstanceOf[Int], s"Settings '$key' is not an integer!")
    option.value.asInstanceOf[Int]
  }

  def getColor(key: String): Color = {
    val option = getOption(key)
    assert(option.value.isInstanceOf[Color], s"Settings '$key' is not a color!")
    option.value.asInstanceOf[Color]
  }
}

case class Setting(
  key: String,
  value: Any,
  name: String = "",
  description: String = "",
) {
  def withValue(newValue: Any): Setting =
    Setting(key, newValue, name, description)
}