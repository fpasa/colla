package xyz.fpasa.colla
package generators

import java.awt.image.BufferedImage

class Vec2Di(val x: Int, val y: Int) {
  def this(component: (Int, Int)) = this(component(0), component(1))

  val w: Int = x
  val h: Int = y

  def +(c: Int): Vec2Di = Vec2Di(x + c, y + c)
  def -(c: Int): Vec2Di = Vec2Di(x - c, y - c)
  def *(c: Int): Vec2Di = Vec2Di(x * c, y * c)
  def /(c: Int): Vec2Di = Vec2Di(x / c, y / c)
  def +(other: Vec2Di): Vec2Di = Vec2Di(x + other.x, y + other.y)
  def -(other: Vec2Di): Vec2Di = Vec2Di(x - other.x, y - other.y)
  def *(other: Vec2Di): Vec2Di = Vec2Di(x * other.x, y * other.y)
  def /(other: Vec2Di): Vec2Di = Vec2Di(x / other.x, y / other.y)

  def withX(x: Int): Vec2Di = Vec2Di(x, y)
  def withXOffset(xOffset: Int): Vec2Di = Vec2Di(x+xOffset, y)
  def withY(y: Int): Vec2Di = Vec2Di(x, y)
  def withYOffset(yOffset: Int): Vec2Di = Vec2Di(x, y+yOffset)

  def isHorizontal: Boolean = x.abs >= y.abs
  def toFloat: Vec2Df = Vec2Df(x.toFloat, y.toFloat)
  def area: Int = x * y
  def minDim(): Int = if (x < y) x else y
  def maxDim(): Int = if (x > y) x else y

  def iter: Iterable[Vec2Di] =
    (0 until y.toInt).flatMap(yi =>
      (0 until x.toInt).map(xi =>
        Vec2Di(xi, yi)
      )
    )

  def iterMax(max: Int): Iterable[Vec2Di] =
    iter.zipWithIndex.takeWhile((vec, idx) => idx < max).unzip.head
}

object Vec2Di {
  def imageDimension(image: BufferedImage): Vec2Di =
    Vec2Di(image.getWidth, image.getHeight)
}

class Vec2Df(val x: Float, val y: Float) {
  def this(component: (Float, Float)) = this(component(0), component(1))

  val w: Float = x
  val h: Float = y

  def +(c: Float): Vec2Df = Vec2Df(x + c, y + c)
  def -(c: Float): Vec2Df = Vec2Df(x - c, y - c)
  def *(c: Float): Vec2Df = Vec2Df(x * c, y * c)
  def /(c: Float): Vec2Df = Vec2Df(x / c, y / c)
  def +(other: Vec2Df): Vec2Df = Vec2Df(x + other.x, y + other.y)
  def -(other: Vec2Df): Vec2Df = Vec2Df(x - other.x, y - other.y)
  def *(other: Vec2Df): Vec2Df = Vec2Df(x * other.x, y * other.y)
  def /(other: Vec2Df): Vec2Df = Vec2Df(x / other.x, y / other.y)

  def withX(x: Float): Vec2Df = Vec2Df(x, y)
  def withXOffset(xOffset: Float): Vec2Df = Vec2Df(x+xOffset, y)
  def withY(y: Float): Vec2Df = Vec2Df(x, y)
  def withYOffset(yOffset: Float): Vec2Df = Vec2Df(x, y+yOffset)

  def isHorizontal: Boolean = x.abs >= y.abs
  def toInt: Vec2Di = Vec2Di(x.toInt, y.toInt)
  def area: Float = x * y
  def minDim(): Float = if (x < y) x else y
  def maxDim(): Float = if (x > y) x else y
}

object Vec2Df {
  def fromAngle(angle: Float): Vec2Df =
    Vec2Df(
      Math.cos(angle.toDouble).toFloat,
      // We need a minus because the sin(x) points upwards,
      // our vector points down, at least in pixel coordinates.
      -Math.sin(angle.toDouble).toFloat,
    )
}