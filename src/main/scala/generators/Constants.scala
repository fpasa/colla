package xyz.fpasa.colla
package generators

import java.awt.image.BufferedImage
import java.awt.{Color, Graphics}

object Constants {
  val CommonGeneratorSettings: Seq[Setting] = Seq(
    Setting(
      "margin", 0,
      "Margin", "Margin size at the border of the collage."
    ),
    Setting(
      "gap", 0,
      "Gap", "Gap between photos."
    ),
    Setting(
      "backgroundColor", Color.WHITE,
      "Background color", "Background color of the collage."
    ),
  )
}
