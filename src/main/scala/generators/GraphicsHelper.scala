package xyz.fpasa.colla
package generators

import java.awt.geom.AffineTransform
import java.awt.{Color, Graphics, Polygon, Shape}
import java.awt.image.BufferedImage
import scala.swing.Graphics2D

class GraphicsHelper(val graphics: Graphics2D) {
  private val PI = Math.PI.toFloat

  def fillBackground(color: Color, collageSize: Vec2Di): Unit = {
    graphics.setColor(color)
    graphics.fillRect(0, 0, collageSize.w, collageSize.h)
  }

  def fitInto(
   image: BufferedImage,
   pos: Vec2Di,
   size: Vec2Di,
   clipShape: Option[Shape] = None,
 ): Unit = {
    val imgSize = Vec2Di.imageDimension(image)

    val ratio = size.toFloat / imgSize.toFloat
    val (origin, finalSize) = if (ratio.isHorizontal) {
      // Target region's aspect ratio is wider than the image's
      // Image need to be center-fitted vertically
      val finalHeight = Math.round(imgSize.h * ratio.x)
      val centerOffset = Math.round((finalHeight - size.h) / 2f)
      (pos.withYOffset(-centerOffset), size.withY(finalHeight))
    } else {
      // Image aspect ratio is higher than the target region's
      // fit center vertically
      val finalWidth = Math.round(imgSize.w * ratio.y)
      val centerOffset = Math.round((finalWidth - size.w) / 2f)
      (pos.withXOffset(-centerOffset), size.withX(finalWidth))
    }

    clipShape match {
      case Some(shape) => graphics.setClip(shape)
      case _ => graphics.setClip(pos.x, pos.y, size.w, size.h)
    }
    graphics.drawImage(image, origin.x, origin.y, finalSize.w, finalSize.h, null)
  }

  def roundedRectangle(pos: Vec2Di, size: Vec2Di, r: Int): Shape = {
    if (r == 0) {
      val end = pos + size
      return new Polygon(
        Array(pos.x, end.x, end.x, pos.x),
        Array(pos.y, pos.y, end.y, end.y),
        4,
      )
    }

    // Ensure the radius is not larger than half the size
    val radius = Math.min(r, (size / 2).minDim())

    val innerSize = size - 2 * radius
    // I started by using log, since log2 does not seem to exist in
    // java's math library, but found that sqrt works better as it
    // results in more points.
    val optimalNumOfPoints = 2 + Math.round(Math.sqrt(radius)).toInt

    val painter = new PolygonPainter()

    painter.moveRel(pos.x + radius, pos.y)
    painter.moveRelX(innerSize.w)
    drawQuarterOfCircle(painter, PI / 2, 0, radius, optimalNumOfPoints)

    painter.moveRelY(innerSize.h)
    drawQuarterOfCircle(painter, 0, -PI / 2, radius, optimalNumOfPoints)

    painter.moveRelX(-innerSize.w)
    drawQuarterOfCircle(painter, -PI / 2, - PI, radius, optimalNumOfPoints)

    painter.moveRelY(-innerSize.h)
    drawQuarterOfCircle(painter, PI, PI / 2, radius, optimalNumOfPoints)

    painter.poly
  }

  private def drawQuarterOfCircle(
    painter: PolygonPainter,
    startAngle: Float,
    endAngle: Float,
    radius: Int,
    numOfPoints: Int,
  ): Unit = {
    // It's going to be difficult to create an arc using relative
    // movements, so I added a method to do absolute movement.
    // In this routine, I calculate the center of the circle and
    // and the simply add points from the start and end angles
    // along the circumference.
    val startVector = Vec2Df.fromAngle(startAngle)
    val center = painter.cursor - (startVector * radius).toInt

    // numOfPoints does not include the initial point
    val angleStep = (endAngle - startAngle) / numOfPoints
    var angle = startAngle;

    for (i <- 0 until numOfPoints) {
      angle += angleStep
      val point = center + (Vec2Df.fromAngle(angle) * radius).toInt
      painter.moveAbs(point.x, point.y)
    }
  }

  def roundedRhombus(center: Vec2Di, size: Vec2Di, r: Int): Shape = {
    val pos = center - size / 2
    val square = roundedRectangle(pos, size, r)
    val rhombus = AffineTransform.getRotateInstance(PI / 4).createTransformedShape(square)
    rhombus
  }
}

private class PolygonPainter {
  val poly = new Polygon()
  var cursor = Vec2Di(0, 0)

  def moveAbs(x: Int, y: Int): Unit = {
    cursor = cursor.withX(x).withY(y)
    poly.addPoint(cursor.x, cursor.y)
  }

  def moveRel(offsetX: Int, offsetY: Int): Unit = {
    cursor = cursor.withXOffset(offsetX).withYOffset(offsetY)
    poly.addPoint(cursor.x, cursor.y)
  }

  def moveRelX(offsetX: Int): Unit = {
    cursor = cursor.withXOffset(offsetX)
    poly.addPoint(cursor.x, cursor.y)
  }

  def moveRelY(offsetY: Int): Unit = {
    cursor = cursor.withYOffset(offsetY)
    poly.addPoint(cursor.x, cursor.y)
  }
}