package xyz.fpasa.colla
package generators

import java.awt.image.BufferedImage
import scala.swing.Graphics2D

abstract class CollageGenerator(
  _collageSize: (Int, Int),
  protected val photos: Seq[BufferedImage],
  protected val settings: Settings,
) {
  assert(photos.nonEmpty, "A PhotoCollage must be initialized with at least an image")

  protected val collageSize: Vec2Di = Vec2Di(_collageSize)

  def createCollage(): BufferedImage = {
    val collage = BufferedImage(collageSize.x, collageSize.y, BufferedImage.TYPE_3BYTE_BGR)
    createCollageBuffer(collage)
    collage
  }

  def withGraphics(collage: BufferedImage)(body: (GraphicsHelper) => Unit): Unit = {
    val graphics = collage.createGraphics
    body(GraphicsHelper(graphics))
    graphics.dispose()
  }

  def setSeed(_seed: Long): Unit = {}

  // Implement these
  protected def createCollageBuffer(collage: BufferedImage): Unit
}