package xyz.fpasa.colla
package generators

import scala.util.Random

object OtherHelpers {
  def choose[T](seq: Seq[T]): T =
    seq(Random.nextInt( seq.length))
}
