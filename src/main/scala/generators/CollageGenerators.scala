package xyz.fpasa.colla
package generators

import java.awt.image.BufferedImage

object CollageGenerators {
  val AvailableCollages: Seq[UICollageGenerator] = Seq(
    UICollageGenerator(
      "Grid",
      classOf[GridGenerator],
      GridGenerator.DefaultSettings,
    ),
    UICollageGenerator(
      "Harmonic",
      classOf[HarmonicGenerator],
      HarmonicGenerator.DefaultSettings,
    ),
    UICollageGenerator(
      "Rhombus",
      classOf[GridRhombusGenerator],
      GridRhombusGenerator.DefaultSettings,
    ),
  )
}

case class UICollageGenerator(
  name: String,
  cls: Class[_ <: CollageGenerator],
  defaultOptions: Settings,
) {
  def newInstance(
     collageSize: (Int, Int),
     photos: Seq[BufferedImage],
     settings: Settings,
  ): CollageGenerator =
    cls
      .getConstructors
      .head
      .newInstance(collageSize, photos, settings)
      .asInstanceOf[CollageGenerator]
}
